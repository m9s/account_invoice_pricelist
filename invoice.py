#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Get, Not, Bool, Equal, Eval, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft')),
}
DEPENDS = ['state']


class Invoice(ModelSQL, ModelView):
    "Invoice"
    _name = "account.invoice"

    pricelist = fields.Many2One('pricelist.pricelist', 'Pricelist',
            on_change=['pricelist'], states=STATES, required=True, domain=[
                ('active', '=', True),
                ('company', '=', Eval('company')),
            ], depends=DEPENDS+['company'])

    def __init__(self):
        super(Invoice, self).__init__()

        self._error_messages.update({
            'pricelist_inactive': 'The selected pricelist is set to inactive! '
                                  'Please select another one!',
            'cust_list_inactive': 'The pricelist of the selected party is set '
                                  'to inactive! Please select another one!',
        })

    def on_change_pricelist(self, vals):
        pricelist_obj = Pool().get('pricelist.pricelist')
        res = {}
        if vals.get('pricelist'):
            pricelist = pricelist_obj.browse(vals['pricelist'])
            if not pricelist.active==True:
                self.raise_user_error('pricelist_inactive')
            else:
                res['currency'] = pricelist.currency.id
        return res

    def on_change_party(self, vals):
        pool = Pool()
        party_obj = pool.get('party.party')
        address_obj = pool.get('party.address')
        account_obj = pool.get('account.account')
        payment_term_obj = pool.get('account.invoice.payment_term')
        company_obj = pool.get('company.company')

        res = super(Invoice, self).on_change_party(vals)
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            if vals.get('type') in ('out_invoice', 'out_credit_note'):
                if party.pricelist_sale:
                    if party.pricelist_sale.active==True:
                        res['pricelist'] = party.pricelist_sale.id
                    else:
                        self.raise_user_warning('inactive_pricelist',
                                'cust_list_inactive')
            elif vals.get('type') in ('in_invoice', 'in_credit_note'):
                if party.pricelist_purchase:
                    if party.pricelist_purchase.active==True:
                        res['pricelist'] = party.pricelist_purchase.id
                    else:
                        self.raise_user_warning('inactive_pricelist',
                                'cust_list_inactive')
        return res

    def _credit(self, invoice):
        '''
        Return values to credit invoice.
        '''
        res = super(Invoice, self)._credit(invoice)
        res['pricelist'] = invoice['pricelist'].id
        return res

Invoice()


class InvoiceLine(ModelSQL, ModelView):
    "Invoice Line"
    _name = "account.invoice.line"

    def __init__(self):
        super(InvoiceLine, self).__init__()
        self.quantity = copy.copy(self.quantity)
        self.product = copy.copy(self.product)
        fields = ['product','quantity', 'unit',
                  '_parent_invoice.currency',
                  '_parent_invoice.pricelist']
        if self.quantity.on_change==None:
            self.quantity.on_change=[]
        for field in fields:
            if field not in self.quantity.on_change:
                self.quantity.on_change += [field]

        if self.product.on_change==None:
            self.product.on_change=[]
        if '_parent_invoice.pricelist' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.pricelist']

        if not self.product.context:
            self.product.context = {
                    'pricelist':
                        Get(Eval('_parent_invoice', {}), 'pricelist'),
                    'currency':
                        Get(Eval('_parent_invoice', {}), 'currency')}
        else:
            if 'pricelist' not in self.product.context:
                self.product.context['pricelist'] = \
                        Get(Eval('_parent_invoice', {}), 'pricelist')
            if 'currency' not in self.product.context:
                self.product.context['currency'] = \
                        Get(Eval('_parent_invoice', {}), 'currency')

        self._reset_columns()
        self._rpc.update({
                          'on_change_quantity': False,
                          })

    def on_change_product(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        currency_obj = pool.get('currency.currency')
        pricelist_obj = pool.get('pricelist.pricelist')
        context = {}

        res = super(InvoiceLine, self).on_change_product(vals)

        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])
        if vals.get('_parent_invoice.currency', vals.get('currency')):
            #TODO check if today date is correct
            currency = currency_obj.browse(vals.get('_parent_invoice.currency',
                    vals.get('currency')))
            context['currency'] = vals.get('_parent_invoice.currency',
                                       vals.get('currency'))

        if vals.get('_parent_invoice.pricelist'):
            context['pricelist'] = vals.get('_parent_invoice.pricelist')

        with Transaction().set_context(context):
            res['unit_price'] = pricelist_obj.get_price([vals.get('product')],
                    quantity=vals.get('quantity', 1))[product.id]

        vals = vals.copy()
        vals['unit_price'] = res['unit_price']
        res['amount'] = self.on_change_with_amount(vals)

        return res

    def on_change_quantity(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        pricelist_obj = pool.get('pricelist.pricelist')
        currency_obj = pool.get('currency.currency')
        res = {}
        context = {}

        if hasattr(super(InvoiceLine, self), 'on_change_quantity'):
            res = super(InvoiceLine, self).on_change_quantity(vals)

        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])
        if vals.get('_parent_invoice.currency'):
            context['currency'] = vals['_parent_invoice.currency']
        if vals.get('unit'):
            context['uom'] = vals['unit']

        if vals.get('_parent_invoice.pricelist'):
            context['pricelist'] = vals.get('_parent_invoice.pricelist')

        with Transaction().set_context(context):
            res['unit_price'] = pricelist_obj.get_price([vals.get('product')],
                    quantity=vals.get('quantity', 1))[product.id]
        return res

InvoiceLine()
