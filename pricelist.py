#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import datetime
from decimal import Decimal
from trytond.modules.company import CompanyReport
from trytond.backend import TableHandler
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Not, Bool, Equal, Eval, Or, And
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
        'readonly': Or(
            Not(Bool(Eval('active'))),
            Bool(Eval('versions')))}
DEPENDS = ['active', 'versions']
STATES_VERSION = {
        'readonly': Not(Bool(Eval('active')))}
DEPENDS_VERSION = ['active']
_TYPES = [
        ('sale', 'Sale'),
        ('purchase', 'Purchase')]


class Pricelist(ModelSQL, ModelView):
    "Pricelist"
    _description = __doc__
    _name = "pricelist.pricelist"

    name = fields.Char('Name', required=True, select=1,
            states={'readonly': Not(Bool(Eval('active')))}, depends=['active'])
    active = fields.Boolean('Active', select=1)
    type = fields.Selection(_TYPES, 'Type', states=STATES, required=True,
            depends=DEPENDS, select=1)
    currency = fields.Many2One('currency.currency', 'Currency',
            required=True, states=STATES, depends=DEPENDS)
    versions = fields.One2Many('pricelist.version', 'pricelist',
            'Pricelist Versions', states={
                'readonly': Or(
                    Not(Bool(Eval('active'))),
                    Not(Bool(Eval('currency'))),
                    Not(Bool(Eval('company')))),
            }, depends=['active', 'currency', 'company'])
    standard = fields.Boolean('Standard', select=2, states={
            'readonly': Not(Bool(Eval('active')))}, depends=['active'])
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, depends=DEPENDS, select=2)

    def __init__(self):
        super(Pricelist, self).__init__()
        self._sql_constraints = [
            ('name_uniq', 'UNIQUE(name)',
             'The name of the pricelist must be unique!')
        ]
        self._constraints += [
            ('_check_standard_pricelist', 'only_one_default'),
        ]
        self._error_messages.update({
            'cycl_categories': 'Could not resolve product category, '
                    'you have defined cyclic categories of products!',
            'only_one_default': 'There can be only one default pricelist by '
                    'type and company!',
            'no_pricelist': 'No pricelist selected. Please select one!',
            'product_not_found': 'No item defined for the selected product, '
                    'neither in the selected pricelist nor in the default '
                    'pricelist! Please add the product to the pricelists!',
            'delete_pricelist': 'You cannot delete activated pricelists!\n'
                    'If you really want delete this pricelist you first '
                    'need to deactivate it.',})

    def default_active(self):
        return True

    def default_standard(self):
        return False

    def default_currency(self):
        company_obj = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            company = company_obj.browse(company)
            return company.currency.id
        return False

    def default_company(self):
        return Transaction().context.get('company') or False

    def default_type(self):
        return Transaction().context.get('pricelist_type') or 'sale'

    def _get_product_args(self, product):
        category_obj = Pool().get('product.category')

        category_ids = []

        category = product.category.id
        while category:
            category_ids.append(category)
            category = category_obj.browse(category)
            category = category.parent.id
            if category in category_ids:
                self.raise_user_error('cycl_categories')
        args1 = ['OR']
        for id in category_ids:
            args1.append(('product_category.id', '=', id))

        args = ['OR', ('product.id', '=', product.id),
               ('product_template.id', '=', product.template.id)]
        args.append(args1)
        return args

    def _get_product_price(self, version_ids, product_id, quantity, args,
            price_type):
        price = False

        if not price and price_type not in ['standard'] and \
                version_ids.get('default'):
            price = self._compute_price(version_ids['default'], product_id,
                    quantity, args, price_type=price_type)
        if not price and version_ids.get('standard'):
            price = self._compute_price(version_ids['standard'], product_id,
                    quantity, args, price_type=price_type)
        return price

    def get_price(self, ids, price_type='default', quantity=1):
        '''
        Return the price for product ids.

        :param ids: the product ids
        :param quantity: the quantity of the products
        :param price_type: 'default': default price computation
                           'standard': price from standardpricelist
        The transaction context can have the keys:
            currency: the currency id for the returned price
            pricelist: the pricelist id
            uom: the unit of measure for the product
            supplier: the supplier of the product (needed?)
            recursion: the number of recursions
            price_date: the date to be used to retrieve pricelist version
        :return: a dictionary with the computed price for each product id
        '''
        pool = Pool()
        version_obj = pool.get('pricelist.version')
        product_obj = pool.get('product.product')

        res = {}


        if isinstance(ids, (int, long)):
            ids = [ids]

        if quantity==False:
            quantity = 1

        pricelist = Transaction().context.get('pricelist') or False

        if not pricelist:
            for id in ids:
                res[id] = Decimal('0.0')
            return res

        version_ids = version_obj.get_versions(pricelist)

        products = product_obj.browse(ids)
        for product in products:
            args = self._get_product_args(product)

            context = {}
            context['default_uom'] = product.default_uom

            with Transaction().set_context(context):
                price = self._get_product_price(version_ids, product.id,
                        quantity, args, price_type)

            if not price:
                res[product.id] = Decimal('0.0')
            else:
                res[product.id] = price
        return res

    def _compute_price(self, ids, product_id, quantity, args, price_type=None):
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        uom_obj = pool.get('product.uom')
        item_obj = pool.get('pricelist.item')
        date_obj = pool.get('ir.date')
        today = date_obj.today()

        if price_type is None:
            price_type = ''

        uom = None
        context_uom = Transaction().context.get('uom')
        context_default_uom = Transaction().context.get('default_uom')
        context_currency = Transaction().context.get('currency')

        if context_uom:
            uom = uom_obj.browse(context_uom)

        if context_currency:
            currency = currency_obj.browse(context_currency)
        else:
            return False

        res = False

        if isinstance(ids, (int, long)):
            ids = [ids]

        for version_id in ids:
            args1 = ['AND']
            args1.append(args)
            args1.append(('pricelist_version.id', '=', version_id))
            item_ids = item_obj.search(args1, order=[('sequence', 'ASC')],
                    limit=1)
            items = item_obj.browse(item_ids)
            if items:
                price = False
                item = items[0]
                price = item_obj.get_item_price(item, product_id, quantity)
                if price:
                    if uom:
                        price = uom_obj.compute_price(context_default_uom,
                                price, uom)
                        # TODO: check, if there could be a correcter date than
                        # today, when computing historical prices?
                        with Transaction().set_context(date=today):
                            res = currency_obj.compute(
                            item.pricelist_version.pricelist.currency.id, price,
                            currency.id, round=False)
                    break
        return res

    def _check_standard_pricelist(self, ids):
        for pricelist in self.browse(ids):
            args = [('type', '=', pricelist.type),
                    ('company', '=', pricelist.company),
                    ('standard', '=', True)]
            standard_pricelist_ids = self.search(args)
            if len(standard_pricelist_ids) > 1:
                return False
        return True

    def delete(self, ids):
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        for pricelist in self.browse(ids):
            if pricelist.active:
                self.raise_user_error('delete_pricelist')
        return super(Pricelist, self).delete(ids)

    def copy(self, ids, default=None):
        int_id = False
        if isinstance(ids, (int, long)):
            int_id = True
            ids = [ids]

        if default is None:
            default = {}
        default = default.copy()
        default['standard'] = False

        new_ids = []
        for pricelist in self.browse(ids):
            default2 = default.copy()
            default2['name'] = pricelist.name + ' (copy)'
            new_id = super(Pricelist, self).copy(pricelist.id,
                    default=default2)
            new_ids.append(new_id)

        if int_id:
            return new_ids[0]
        return new_ids

Pricelist()


class PricelistVersion(ModelSQL, ModelView):
    "Pricelist Version"
    _description = __doc__
    _name = "pricelist.version"

    name = fields.Char('Name', required=True, select=1,
            states=STATES_VERSION, depends=DEPENDS_VERSION)
    pricelist = fields.Many2One('pricelist.pricelist', 'Pricelist',
            required=True, select=1, ondelete='CASCADE',
            states=STATES_VERSION, depends=DEPENDS_VERSION)
    active = fields.Boolean('Active', select=1)
    items = fields.One2Many('pricelist.item', 'pricelist_version',
            'Pricelist Items', states=STATES_VERSION, depends=DEPENDS_VERSION)
    date_start = fields.Date('Start Date', required=True,
            states=STATES_VERSION, depends=DEPENDS_VERSION)
    date_end = fields.Date('End Date', states=STATES_VERSION,
            depends=DEPENDS_VERSION)

    def __init__(self):
        super(PricelistVersion, self).__init__()
        self._sql_constraints = [
            ('name_uniq', 'UNIQUE(name)',
             'The name of the pricelist version must be unique!')
        ]
        self._constraints += [
            ('_check_date', 'date_overlap'),
            ('_check_start_end', 'start_end')
        ]
        self._error_messages.update({
            'no_pl_version': 'No active or valid version in one ' +
                                'of the allocated pricelists!\n' +
                                'Please create or activate one.',
            'date_overlap': 'Date periods of pricelist versions ' +
                                'can not overlap!',
            'delete_version': 'You can not delete activated pricelist ' +
                              'versions!\n' +
                              'If you really want delete this version you ' +
                              'first need to deactivate it!',
            'no_default_pricelist': 'Default pricelist for your company not '
                                    'found! Please create one!',
            'start_end': 'The end date must be after the start date!'
        })

    def default_active(self):
        return True

    def _check_date(self, ids):
        cursor = Transaction().cursor
        for version in self.browse(ids):
            if not version.active:
                continue
            if not version.date_start:
                date_start = '0001-01-01'
            else:
                date_start = version.date_start
            if not version.date_end:
                date_end = '0001-01-01'
            else:
                date_end = version.date_end
            cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE ((date_start <= %s AND %s <= date_end ' \
                            'AND date_end IS NOT NULL) ' \
                        'OR (date_end IS NULL AND date_start IS NOT NULL ' \
                            'AND date_start <= %s) ' \
                        'OR (date_start IS NULL AND date_end IS NOT NULL ' \
                            'AND %s <= date_end) ' \
                        'OR (date_start IS NULL AND date_end IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND date_start IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND date_end IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND %s = \'0001-01-01\') ' \
                        'OR (%s = \'0001-01-01\' AND date_start <= %s) ' \
                        'OR (%s = \'0001-01-01\' AND %s <= date_end)) ' \
                        'AND pricelist = %s ' \
                        'AND active ' \
                        'AND id <> %s', [str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_end),
                            str(date_start),
                            version.pricelist.id,
                            version.id])
            if cursor.fetchall():
                return False
        return True

    def _check_start_end(self, ids):
        res = True
        for version in self.browse(ids):
            if version.date_end is not None:
                if version.date_end < version.date_start:
                    res = False
        return res

    def get_version(self, pricelist_id, date=False):
        cursor = Transaction().cursor
        date_obj = Pool().get('ir.date')

        date = date or Transaction().context.get('price_date') or \
                str(date_obj.today())
        cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE pricelist = %s AND active=True ' \
                        'AND (date_start IS NULL OR date_start <= %s) ' \
                        'AND (date_end IS NULL OR date_end >= %s) ' \
                    'ORDER BY id LIMIT 1', [pricelist_id, date, date])
        version = cursor.dictfetchone()
        if not version:
            return False
        return version['id']

    def get_versions(self, pricelist_id, date=False):
        pricelist_obj = Pool().get('pricelist.pricelist')
        version_ids = {}
        version_ids['default'] = self.get_version(pricelist_id, date=date)
        if not version_ids['default']:
            self.raise_user_error('no_pl_version')

        #Detect Standard Pricelist
        pricelist = pricelist_obj.browse(pricelist_id)
        args = [('standard', '=', True),
                ('type', '=', pricelist.type),
                ('company', '=', pricelist.company.id)]
        standard_pricelist_ids = pricelist_obj.search(args, limit=1)
        if not standard_pricelist_ids:
            self.raise_user_error('no_default_pricelist')
        version_ids['standard'] = self.get_version(standard_pricelist_ids[0],
                date=date)
        if not version_ids['standard']:
            self.raise_user_error('no_pl_version')

        return version_ids

    def delete(self, ids):
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        for version in self.browse(ids):
            if version.active==True:
                self.raise_user_error('delete_version')
        return super(PricelistVersion, self).delete(ids)

    def copy(self, ids, default=None):
        int_id = False
        if isinstance(ids, (int, long)):
            int_id = True
            ids = [ids]

        if default is None:
            default = {}

        new_ids = []
        for version in self.browse(ids):
            default2 = default.copy()
            default2['name'] = version.name + ' (copy)'
            new_id = super(PricelistVersion, self).copy(version.id,
                    default=default2)
            new_ids.append(new_id)

        if int_id:
            return new_ids[0]
        return new_ids

PricelistVersion()


class PricelistItem(ModelSQL, ModelView):
    "Pricelist Item"
    _description = __doc__
    _name = "pricelist.item"
    _rec_name = 'type'

    pricelist_version = fields.Many2One('pricelist.version',
            'Pricelist Version', required=True, ondelete='CASCADE')
    type = fields.Selection([
            ('fix', 'Fix Price'),
            ('markup', 'Markup'),
            ('blockprice', 'Block Price')],
            'Calculation Type', required=True, select=1, depends=['calc_value'])
    parent_pricelist = fields.Function(fields.Integer('Parent Pricelist'),
            'get_parent_pricelist')
    calc_value = fields.Function(fields.Numeric('Calculation Value',
            on_change_with=['type','markup','amount_fix','block_prices']),
            'get_calc_value')
    sequence = fields.Integer('Priority', required=True)
    markup = fields.Numeric('Markup Percentage', digits=(16, 8),
            states={
                'invisible': Not(Equal(Eval('type'), 'markup')),
                'required': Equal(Eval('type'), 'markup'),
            }, depends=['calc_value', 'type'])
    amount_fix = fields.Numeric('Fix Price', digits=(16, 4),
            states={
                'invisible': Not(Equal(Eval('type'), 'fix')),
                'required': Equal(Eval('type'), 'fix'),
            }, depends=['calc_value', 'type'])
    product = fields.Many2One('product.product', 'Product',
            on_change=['product'], ondelete='CASCADE',
            depends=['product_template','product_category'], states={
                'required': And(
                    Not(Bool(Eval('product_template'))),
                    Not(Bool(Eval('product_category'))))})
    product_template = fields.Many2One('product.template', 'Product Template',
            on_change=['product_template'], ondelete='CASCADE',
            depends=['product_template', 'product_category', 'product'],
            states={
                'required': And(
                    Not(Bool(Eval('product'))),
                    Not(Bool(Eval('product_category'))))})
    product_category = fields.Many2One('product.category', 'Product Category',
            on_change=['product_category'], ondelete='CASCADE',
            states={'invisible': Not(Equal(Eval('type'), 'markup')),
                    'required': And(
                        Not(Bool(Eval('product'))),
                        Not(Bool(Eval('product_template')))),
            }, depends=['type', 'product', 'product_template'])
    pricelist = fields.Many2One('pricelist.pricelist', 'Purchase Pricelist',
            states={
                'invisible': Not(Equal(Eval('type'), 'markup')),
                'required': Equal(Eval('type'), 'markup'),
            }, help='Select a base pricelist for markup', domain=[
                ('type', '=', 'purchase'),
                ('id', '!=', Eval('parent_pricelist')),
            ], context={'pricelist_type': 'purchase'},
            depends=['parent_pricelist', 'type'])
    block_prices = fields.One2Many('pricelist.item.block_price',
            'pricelist_item', 'Block Prices', states={
                'invisible': Not(Equal(Eval('type'), 'blockprice'))},
            depends=['calc_value', 'type'])
    date_start = fields.Function(fields.Date('Start Date'), 'get_date')
    date_end = fields.Function(fields.Date('End Date'), 'get_date')

    def __init__(self):
        super(PricelistItem, self).__init__()
        self._order[0] = ('sequence', 'ASC')
        self._constraints += [
            ('_check_base_quantity', 'missing_base_quantity'),
            ('_check_item_unique', 'double_item')
        ]
        self._error_messages.update({
            'missing_base_quantity': 'There must be a line with a base ' +
                                'quantity of value 1 for Block Prices! ' +
                                'Please create one.',
            'double_item': 'You can only have one entry by product, template ' +
                           'or category in a pricelist version!',
        })

    def init(self, module_name):
        super(PricelistItem, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)
        # Remove old name field
        if table.column_exist('name'):
            table.drop_column('name', exception=True)

    def default_type(self):
        return 'fix'

    def default_sequence(self):
        return 10

    def default_amount_fix(self):
        return Decimal('0.0')

    def on_change_product(self, vals):
        product_obj = Pool().get('product.product')
        res = {}
        if vals.get('product'):
            product = product_obj.browse(vals['product'])
            res['name'] = product.name
        res['product_template'] = False
        res['product_category'] = False
        return res

    def on_change_product_template(self, vals):
        template_obj = Pool().get('product.template')
        res = {}
        if vals.get('product_template'):
            template = template_obj.browse(vals['product_template'])
            res['name'] = template.name
        res['product'] = False
        res['product_category'] = False
        return res

    def on_change_product_category(self, vals):
        category_obj = Pool().get('product.category')
        res = {}
        if vals.get('product_category'):
            category = category_obj.browse(vals['product_category'])
            res['name'] = category.name
        res['product'] = False
        res['product_template'] = False
        return res

    def on_change_with_calc_value(self, vals):
        if vals.get('type'):
            if vals.get('type')=='fix':
                return vals.get('amount_fix')
            elif vals.get('type')=='markup':
                return vals.get('markup')
        return Decimal('0.0')

    def get_calc_value(self, ids, name):
        res = {}
        for line in self.browse(ids):
            if line.type=='fix':
                res[line.id] = line.amount_fix
            elif line.type=='markup':
                res[line.id] = line.markup
            else:
                res[line.id] = Decimal('0.0')
        return res

    def get_date(self, ids, name):
        res = {}
        for item in self.browse(ids):
            res[item.id] = eval('item.pricelist_version.'+name)
        return res

    def get_parent_pricelist(self, ids, name):
        res = {}
        for item in self.browse(ids):
            res[item.id] = item.pricelist_version.pricelist.id
        return res

    def _check_base_quantity(self, ids):
        res = True
        for item in self.browse(ids):
            if item.block_prices:
                has_base_quantity = False

                for block_item in item.block_prices:
                    if block_item.quantity==1:
                        has_base_quantity = True

                if not has_base_quantity:
                    res = False
        return res

    def _check_item_unique(self, ids):
        product_obj = Pool().get('product.product')
        res = True
        for item in self.browse(ids):
            args2 = []
            if item.product:
                args2 = ('product', '=', item.product.id)
            elif item.product_category:
                args2 = ('product_category', '=', item.product_category.id)
            elif item.product_template:
                args2 = ['OR',
                        ('product_template', '=', item.product_template.id)]
                args3 = [('template', '=', item.product_template.id)]
                product_ids = product_obj.search(args3)
                for id in product_ids:
                    args2.append(('product', '=', id))
            args = [('pricelist_version', '=', item.pricelist_version.id),
                    args2]
            item_ids = self.search(args)
            if len(item_ids)>1:
                res = False
        return res

    def get_item_price(self, item, product_id, quantity):
        pricelist_obj = Pool().get('pricelist.pricelist')
        block_price_obj = Pool().get('pricelist.item.block_price')

        price = False
        context = {}
        context_recursion = Transaction().context.get('recursion') or 0


        if item.type=='fix':
            price = item.amount_fix
        elif item.type=='markup':
            if context_recursion > 5:
                return False

            context['pricelist'] = item.pricelist.id
            context['currency'] = item.pricelist.currency.id
            context['recursion'] = context_recursion + 1
            with Transaction().set_context(context):
                price = pricelist_obj.get_price(product_id,
                        quantity=quantity)[product_id]
            price = (price + price * item.markup / 100)
        elif item.type=='blockprice':
            if quantity==0:
                quantity = 1
            args3 = [('pricelist_item', '=', item.id)]
            block_item_ids = block_price_obj.search(args3, order=[
                    ('quantity', 'DESC')])
            block_items = block_price_obj.browse(block_item_ids)
            for block_item in block_items:
                if block_item.quantity <= quantity:
                    price = block_item.unit_price
                    break

        return price

PricelistItem()


class PricelistItemBlockPrice(ModelSQL, ModelView):
    "Pricelist Item Blockprice"
    _name = 'pricelist.item.block_price'
    _description = __doc__
    _rec_name = 'quantity'

    pricelist_item = fields.Many2One('pricelist.item', 'Pricelist Item',
            ondelete='CASCADE', required=True)
    quantity = fields.Float('Base Quantity', required=True,
            help='Please enter the lowest quantity allowed for this block.\n' +
                 'There must be one base line with quantity of value 1 .')
    unit_price = fields.Numeric('Unit Price', required=True, digits=(16, 4))

    def __init__(self):
        super(PricelistItemBlockPrice, self).__init__()
        self._order.insert(0, ('quantity', 'ASC'))

PricelistItemBlockPrice()


class PricelistReport(CompanyReport):
    _name = 'pricelist.pricelist'

    def get_version(self, pricelist_id):
        version_obj = Pool().get('pricelist.version')
        return version_obj.get_version(pricelist_id)

    def parse(self, report, objects, datas, localcontext=None):
        pool = Pool()
        version_obj = pool.get('pricelist.version')
        pricelist_obj = pool.get('pricelist.pricelist')
        product_obj = pool.get('product.product')

        objects = []
        if localcontext is None:
            localcontext = {}

        localcontext = localcontext.copy()
        pricelist_id = datas.get('id', False)
        localcontext.setdefault('pricelist', pricelist_obj.browse(pricelist_id))
        with Transaction().set_context(**localcontext):
            version_id = self.get_version(pricelist_id)
        if version_id:
            version = version_obj.browse(version_id)
        localcontext.setdefault('pricelist_version', version)
        args = [('active', '=', True)]
        with Transaction().set_context(**localcontext):
            product_ids = product_obj.search(args)
        products = product_obj.browse(product_ids)
        localcontext2 = {
            'currency': version.pricelist.currency.id,
            'pricelist': version.pricelist.id,
            'pricelist_version': version.id,
            }
        with Transaction().set_context(**localcontext2):
            prices = pricelist_obj.get_price(product_ids)
        for product in products:
            if prices[product.id]:
                objects.append({
                        'name': product.name,
                        'price': prices[product.id],
                        'code': product.code or ''})
        return super(PricelistReport, self).parse(report, objects, datas,
                localcontext)

PricelistReport()


class CopyPricelistVersionInit(ModelView):
    'Copy Pricelist Version Init'
    _description = __doc__
    _name = 'pricelist.pricelist.copy_pricelist_version.init'
    _description = __doc__

    date = fields.Date('Please select a start date \n' +
                       'for the new Pricelist Version(s)')

    def default_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

CopyPricelistVersionInit()

class CopyPricelistVersion(Wizard):
    'Copy Pricelist Version'
    _name = 'pricelist.pricelist.copy_pricelist_version'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'pricelist.pricelist.copy_pricelist_version.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('copy', 'OK', 'tryton-ok', True),
                ],
            },
        },
        'copy': {
            'result': {
                'type': 'action',
                'action': '_copy',
                'state': 'end',
            },
        },
    }

    def _copy(self, datas):
        pool = Pool()
        version_obj = pool.get('pricelist.version')
        item_obj = pool.get('pricelist.item')
        block_price_obj = pool.get('pricelist.item.block_price')

        for pricelist_id in datas['ids']:
            version_id = version_obj.get_version(pricelist_id)
            if not version_id:
                continue

            data = {}
            ids = []
            data['date_end'] = (datas['form']['date'] -
                    datetime.timedelta(days=1))

            version_obj.write(version_id, data)
            version = version_obj.browse(version_id)
            data = {}
            data['date_start'] = datas['form']['date']
            data['name'] = version.name + ' (copy)'
            data['pricelist'] = pricelist_id

            new_id = version_obj.create(data)
            ids.append(new_id)

            args = [('pricelist_version', '=', version_id)]
            item_ids = item_obj.search(args)
            items = item_obj.browse(item_ids)

            for item in items:
                data = {}
                data['type'] = item.type
                data['product_category'] = item.product_category
                data['product'] = item.product
                data['product_template'] = item.product_template
                data['sequence'] = item.sequence
                data['markup'] = item.markup
                data['amount_fix'] = item.amount_fix
                data['pricelist_version'] = new_id
                data['pricelist'] = item.pricelist
                new_item_id = item_obj.create(data)
                args = [('pricelist_item', '=', item.id)]
                block_price_ids = block_price_obj.search(args)
                if block_price_ids:
                    block_prices = block_price_obj.browse(block_price_ids)
                    for block_price in block_prices:
                        data = {}
                        data['quantity'] = block_price.quantity
                        data['unit_price'] = block_price.unit_price
                        data['pricelist_item'] = new_item_id
                        block_price_obj.create(data)
        return ids

CopyPricelistVersion()
