#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool
from trytond.transaction import Transaction
from decimal import Decimal
from trytond.pool import Pool


class Template(ModelSQL, ModelView):
    _name = 'product.template'

    cost_price = fields.Property(fields.Numeric('Cost Price', readonly=True,
            digits=(16, 4)))
    customer_price = fields.Function(fields.Numeric('Customer Price'),
            'get_product_price')
    standard_price = fields.Function(fields.Numeric('Standard Price'),
            'get_product_price')

    def get_product_price(self, ids, name):
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = {}
        price_type = None

        context = {}

        if (Transaction().context.get('pricelist')
                and context.get('currency')):
            if name=='standard_price':
                price_type='standard'
            products = self.browse(ids)
            for product in products:
                res[product.id] = pricelist_obj.get_price([product.id],
                        quantity=1, price_type=price_type)[product.id]
        else:
            for id in ids:
                res[id] = Decimal('0.0')

        return res

Template()


class Product(ModelSQL, ModelView):
    _name = 'product.product'

    pricelist_items = fields.One2Many('pricelist.item', 'product',
            'Pricelist Items', states={
                'readonly': Not(Bool(Eval('active'))),
            }, depends=['active'])

Product()
