#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Pricelist',
    'name_de_DE': 'Fakturierung Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Define pricelists for parties
    - Prices can be configured as fixed prices, as block prices and
      as markup on products or product categories.
''',
    'description_de_DE': '''
    - Ermöglicht die Definition von Preislisten für Parteien.
    - Es können entweder feste Preise, Staffelpreise oder ein
      Handelsaufschlag für Artikel oder Artikelkategorien konfiguriert werden.
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
        'pricelist.xml',
        'party.xml',
        'invoice.xml',
        'product.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
