#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.backend import TableHandler
from trytond.pyson import Not, Bool, Equal, Eval, Or, And, PYSONEncoder
from trytond.transaction import Transaction
import datetime

STATES = {
    'readonly': Not(Bool(Eval('active'))),
}
DEPENDS = ['active']


class Party(ModelSQL, ModelView):
    _name = "party.party"

    pricelist_sale = fields.Property(fields.Many2One('pricelist.pricelist',
            'Pricelist Sale', domain=[
                ('type', '=', 'sale'),
                ('active', '=', True),
                ('company', '=', Eval('company')),
            ], select=1, states=STATES, depends=DEPENDS+['company']))
    pricelist_purchase = fields.Property(fields.Many2One('pricelist.pricelist',
            'Pricelist Purchase', domain=[
                ('type', '=', 'purchase'),
                ('active', '=', True),
                ('company', '=', Eval('company')),
            ], select=1, states=STATES, depends=DEPENDS+['company']))

    def init(self, module_name):
        # Migration of pricelist many2one to property
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)
        if table.column_exist('pricelist_sale'):
            cursor.execute("INSERT INTO ir_property (create_date, create_uid, "
                "res, value, name, field, company) "
                "SELECT CURRENT_TIMESTAMP, "
                "0, "
                "'party.party,' || id AS res, "
                "'pricelist.pricelist,' || pricelist_sale, "
                "'pricelist_sale', "
                "(SELECT id FROM ir_model_field WHERE name = 'pricelist_sale' "
                "AND module = %s), "
                "(SELECT id FROM company_company WHERE id = 1) "
                "FROM party_party WHERE pricelist_sale IS NOT NULL",
                (module_name,))

        if table.column_exist('pricelist_purchase'):
            cursor.execute("INSERT INTO ir_property (create_date, create_uid, "
                "res, value, name, field, company) "
                "SELECT CURRENT_TIMESTAMP, "
                "0, "
                "'party.party,' || id, "
                "'pricelist.pricelist,' || pricelist_purchase, "
                "'pricelist_purchase', "
                "(SELECT id FROM ir_model_field WHERE name = 'pricelist_purchase' "
                "AND module = %s), "
                "(SELECT id FROM company_company WHERE id = 1) "
                "FROM party_party WHERE pricelist_purchase IS NOT NULL",
                (module_name,))

        for field in ('pricelist_sale', 'pricelist_purchase'):
            if table.column_exist(field):
                table.drop_column(field, exception=True)

        super(Party, self).init(module_name)

Party()
